public class Card {

    enum Value { DOI(2), TREI(3), PATRU(4), CINCI(5), SASE(6), SAPTE(7), OPT(8), NOUA(9), ZECE(10),
        J(10), Q(10), K(10), A(1);

        int puncte;

        Value(int puncte) {
            this.puncte = puncte;
        }
    }

    enum Suit { INIMA_NEAGRA, TREFLA, INIMA_ROSIE, ROMB }

    private int id;
    private Value valoare;
    private Suit culoare;

    Card(int id, Value valoare, Suit culoare) {
        this.id = id;
        this.valoare = valoare;
        this.culoare = culoare;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Value getValoare() {
        return valoare;
    }

    public void setValoare(Value valoare) {
        this.valoare = valoare;
    }

    public Suit getCuloare() {
        return culoare;
    }

    public void setCuloare(Suit culoare) {
        this.culoare = culoare;
    }

    @Override
    public String toString() {
        return valoare + " de " + culoare;
    }
}
