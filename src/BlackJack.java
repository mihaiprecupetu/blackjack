import java.util.*;

public class BlackJack {

    private static final int THRESHOLD = 17;
    private static final int BLACK_JACK = 21;

    public static void main(String[] args) {
        List<Card> pachetCarti = new ArrayList<>();
        List<Card> cartiJucator = new ArrayList<>();
        List<Card> cartiDealer = new ArrayList<>();

        initializare(pachetCarti, cartiJucator, cartiDealer);

        System.out.println("Carti dealer: ");
        System.out.println(cartiDealer.get(0));
        System.out.println("Carte ascunsa");

        System.out.println();
        System.out.println("Carti jucator: ");
        cartiJucator.forEach(card -> System.out.println(card.toString()));

        if (isBlackJack(cartiJucator) && isBlackJack(cartiDealer)) {
            System.out.println();
            System.out.println("Remiza");
            return;
        }

        System.out.println();
        System.out.println("Runda jucator: ");
        while (faSuma(cartiJucator) <= BLACK_JACK) {
            if (isBlackJack(cartiJucator)) {
                System.out.println("Black Jack Jucator!!");
                break;
            }
            Scanner scanner = new Scanner(System.in);
            System.out.println("Inca o carte? (Y/N)");
            String text = scanner.nextLine();

            if (text.equalsIgnoreCase("Y")){
                Card carte = daCarteRandom(pachetCarti);
                cartiJucator.add(carte);
                System.out.println(carte.toString());
            } else if (text.equalsIgnoreCase("N")) {
                break;
            }
        }

        int sumaJucator = faSuma(cartiJucator);

        System.out.println();
        if (sumaJucator <= BLACK_JACK) {
            System.out.println("Runda dealer: ");
            System.out.println("Carte ascunsa: " + cartiDealer.get(1));

            while (faSuma(cartiDealer) < THRESHOLD) {
                Card carte = daCarteRandom(pachetCarti);
                cartiDealer.add(carte);
                System.out.println(carte.toString());
            }
            if (isBlackJack(cartiDealer)) {
                System.out.println("Black Jack Dealer!!");
            }
        } else {
            System.out.println("Dealerul Castiga !!");
            return;
        }

        int sumaDealer = faSuma(cartiDealer);

        System.out.println();

        System.out.println("Rezultate finale: ");
        System.out.println("Suma jucator: " + sumaJucator);
        System.out.println("Suma dealer: " + sumaDealer);

        if (sumaDealer == sumaJucator) {
            System.out.println("Remiza");
            return;
        }
        System.out.println();
        if (castigaJucatorul(sumaJucator, sumaDealer)) {
            System.out.println("Jucatorul Castiga !!");
        } else {
            System.out.println("Dealerul Castiga !!");
        }

    }

    private static void initializare(List<Card> pachetCarti, List<Card> cartiJucator, List<Card> cartiDealer) {
        incarca(pachetCarti);

        for (int i=0; i<2; i++) {
            cartiJucator.add(daCarteRandom(pachetCarti));
            cartiDealer.add(daCarteRandom(pachetCarti));
        }
    }

    private static boolean castigaJucatorul(int sumaJucator, int sumaDealer) {
        return sumaJucator <= BLACK_JACK && (sumaDealer > BLACK_JACK || sumaDealer < sumaJucator);
    }

    private static boolean isBlackJack(List<Card> carti) {
        return faSuma(carti) == BLACK_JACK;
    }

    private static int faSuma(List<Card> carti) {
        int suma = 0;
        boolean containsAce = false;

        for (Card card : carti) {
            if (card.getValoare().equals(Card.Value.A)){
                containsAce = true;
            }

            suma = suma + card.getValoare().puncte;
        }

        if (containsAce && suma <= 11) {
           suma += 10;
        }

        return suma;
    }

    private static Card daCarteRandom(List<Card> pachetCarti) {
        Random random = new Random();
        return pachetCarti.remove(random.nextInt(pachetCarti.size()));
    }

    private static void incarca(List<Card> pachetCarti) {
        int i = 1;
        for (Card.Value value : Card.Value.values()) {
            for (Card.Suit suit : Card.Suit.values()) {
                pachetCarti.add(new Card(i, value, suit));
                i++;
            }
        }
    }
}
